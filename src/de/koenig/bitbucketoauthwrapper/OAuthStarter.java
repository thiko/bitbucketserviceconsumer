package de.koenig.bitbucketoauthwrapper;
import java.io.IOException;
import java.security.GeneralSecurityException;

public class OAuthStarter {

    /**
     * <ul>
     * <li>Param 0: Consumerkey</li>
     * <li>Param 1: Consumersecret</li>
     * </ul>
     * 
     * @param args
     * @throws GeneralSecurityException
     * @throws IllegalArgumentException
     * @throws IOException 
     */
    public static void main(String... args) throws IllegalArgumentException,
            GeneralSecurityException, IOException {
        
        if (args.length < 2) {
            throw new IllegalArgumentException(
                    "Beim Aufruf sind folgende Parameter notwendig: Consumerkey, Consumersecret");
        } else {
            BitbucketOAuthWrapper wrapper = new BitbucketOAuthWrapper(args[0],args[1]);
            wrapper.requestToken();
        }
    }
}
