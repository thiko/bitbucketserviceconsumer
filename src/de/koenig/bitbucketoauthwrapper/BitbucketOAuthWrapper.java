package de.koenig.bitbucketoauthwrapper;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;


/**
 * OAuthWrapper for authentification at bitbucket via oauth v1.0.
 * <ul>
 * <li>Step 1. Create an OAuth key and secret (on bitbucket.com)</li>
 * <li>Step 2. Request a request token from Bitbucket</li>
 * <li>Step 3. Redirect the user to Bitbucket to authorize your application</li>
 * <li>Step 4. Request an Access Token
 * </ul>
 * @author tkoenig
 */
public class BitbucketOAuthWrapper {

    private static final String HMACSHA1 = "HMACSHA1";
    
    private final String baseUrl = "https://bitbucket.org/!api/1.0/oauth/"; 
    private String consumerKey = "";
    private String consumerSecret = "";
    private final String callbackUrl = "http://example.com/";
    
    private HttpClient client;
    
//    private String requestTokenUrl = "oauth";
//    private String accessTokenUrl = "oauth";
//    private String authorizationUrl = "OAuth.action?oauth_token=%s";
//    private String tokens;
    
    
    public BitbucketOAuthWrapper(final String consumerKey,final String consumerSecret) {
        this.client = new HttpClient();
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
    }
    
    /**
     * Get Requesttoken from bitbucket (see Step 2 in class description)
     * @return Requesttoken
     * @throws IllegalArgumentException
     * @throws GeneralSecurityException
     * @throws IOException 
     */
    public String requestToken() throws IllegalArgumentException, GeneralSecurityException, IOException {
        
        StringBuilder requestUrl = new StringBuilder(baseUrl);
        requestUrl.append("request_token");
        
        PostMethod method = new PostMethod(requestUrl.toString());

        method.addParameter("oauth_version", "1.0");
        method.addParameter("oauth_consumer_key", consumerKey);
        method.addParameter("oauth_nonce", String.valueOf(new Random().nextLong()));
        method.addParameter("oauth_signature", computeSignature(this.baseUrl, consumerSecret));
        // HMAC-SHA1 or PLAINTEXT
        method.addParameter("oauth_signature_method", HMACSHA1);
        method.addParameter("oauth_timestamp", String.valueOf(new Date().getTime()/1000));
        method.addParameter("oauth_callback", callbackUrl);
        
        System.out.println(method.getRequestBodyAsString());
        int returnCode = client.executeMethod(method);
            
        System.out.println(returnCode);
        System.out.println(method.getResponseBodyAsString());
        method.releaseConnection();

        // FIXME: Should return request-token
        return "";
    }
        

    
    /**
     * Generate Signatur with HMACSHA1-Algorithm.
     * 
     * @param baseString - Basestring of Restservice (e.g. https://bitbucket.org/!api/1.0/oauth/)
     * @param keyString - The consumer secret (from bitbucket)
     * @return Encoded Signatur
     * 
     * @throws GeneralSecurityException
     * @throws UnsupportedEncodingException
     */
    private String computeSignature(String baseString, String keyString) throws GeneralSecurityException, UnsupportedEncodingException {

        SecretKey secretKey = null;

        byte[] keyBytes = keyString.getBytes();
        secretKey = new SecretKeySpec(keyBytes, HMACSHA1);

        Mac mac = Mac.getInstance(HMACSHA1);
        mac.init(secretKey);

        byte[] text = baseString.getBytes();

        return new String(Base64.encodeBase64(mac.doFinal(text))).trim();
    }
    
}
